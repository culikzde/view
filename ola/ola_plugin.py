
# cmm_plugin.py

from __future__ import print_function

import os, sys, importlib, inspect

from util import import_qt_modules
import_qt_modules (globals ())

from util import opts
from env import CodePlugin

# --------------------------------------------------------------------------

class Plugin (CodePlugin) :

   def __init__ (self, main_window) :
       super (Plugin, self).__init__ (main_window)
       menu = self.win.addTopMenu (self.mod.title)

   # -----------------------------------------------------------------------

   def olaGenerateParser (self) :
       "called from tools.cfg menu item"
       self.onlyParser ()

   # -----------------------------------------------------------------------

   def setup (self) :
       "setup grammar, parser and compiler file name"
       self.grammarFileName = "ola/ola.g"


# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
